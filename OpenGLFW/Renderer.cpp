#include "glm/glm.hpp"

#include "Renderer.h"
#include "Shader.h"
#include "Camera.h"
#include "GameObject.h"
#include "Light.h"

using namespace glm;

extern void printGLError(const char* marker);

void IRenderer::addGameObject(GameObject* gameObject)
{
	gameObjects.push_back(gameObject);
}

void IRenderer::setShader(Shader* shader)
{
	this->shader = shader;
}


inline void PhongRenderer::setLights(mat4 modelViewMatrix, mat3 normalMatrix)
{
	char uniformName[100];
	// First we transform lights
	int dirLightsCount = 0;
	auto directionalLights = lightManager.getDirectionalLights(); // Copy of directinalLights vector from LightManager
	for (DirectionalLight& dirLight : directionalLights) {
		dirLight.direction = normalize(normalMatrix * dirLight.direction);
		sprintf(uniformName, "directionalLights[%i].direction", dirLightsCount);
		shader->setUniformVec3(uniformName, &dirLight.direction.x);
		sprintf(uniformName, "directionalLights[%i].diffuse", dirLightsCount);
		shader->setUniformVec3(uniformName, &dirLight.diffuse.x);
		sprintf(uniformName, "directionalLights[%i].specular", dirLightsCount);
		shader->setUniformVec3(uniformName, &dirLight.specular.x);
		dirLightsCount++;
	}
	auto pointLights = lightManager.getPointLights();
	int pointLightsCount = 0;
	for (PointLight& pointLight : lightManager.getPointLights()) {
		vec4 transformedLightPosition = vec4(pointLight.position, 1.f);
		transformedLightPosition = modelViewMatrix * transformedLightPosition;
		pointLight.position = vec3(transformedLightPosition);
		sprintf(uniformName, "pointLights[%i].position", pointLightsCount);
		shader->setUniformVec3(uniformName, &pointLight.position.x);
		sprintf(uniformName, "pointLights[%i].linearAttenuation", pointLightsCount);
		shader->setUniformFloat(uniformName, pointLight.linearAttenuation);
		sprintf(uniformName, "pointLights[%i].diffuse", pointLightsCount);
		shader->setUniformVec3(uniformName, &pointLight.diffuse.x);
		sprintf(uniformName, "pointLights[%i].specular", pointLightsCount);
		shader->setUniformVec3(uniformName, &pointLight.specular.x);
		pointLightsCount++;
	}

	shader->setUniformInt("dirLightsCount", directionalLights.size());
	shader->setUniformInt("pointLightsCount", pointLights.size());
}

inline void PhongRenderer::setMaterial(GameObject* gameObject)
{
	Material* m = gameObject->getMaterial();
	shader->setUniformVec3("material.diffuse", &m->diffuse.x);
	shader->setUniformVec3("material.specular", &m->specular.x);
	shader->setUniformFloat("material.shininess", m->shininess);
}

void PhongRenderer::render(Camera* camera)
{
	shader->use();

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	for (auto object : gameObjects) {

		mat4 modelView = camera->getView() * object->getModelMatrix();
		mat4 modelViewProjection = camera->getProjection() * modelView;
		mat3 normalMatrix = mat3(transpose(inverse(modelView)));
		shader->setUniformMat4("modelViewProjection", &modelViewProjection[0][0]);
		shader->setUniformMat4("modelView", &modelView[0][0]);
		shader->setUniformMat3("normalMatrix", &normalMatrix[0][0]);

		setLights(modelView, normalMatrix);
		setMaterial(object);

		for (Mesh* mesh : object->getModel()->meshes) {

			glBindBuffer(GL_ARRAY_BUFFER, mesh->VBO);
			//printGLError("glBindBuffer(Array)");
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

			glBindBuffer(GL_ARRAY_BUFFER, mesh->NBO);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
			
			glBindBuffer(GL_ARRAY_BUFFER, mesh->TCBO);
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
			
			// Set texture unit 0 as currently active texture unit
			glActiveTexture(GL_TEXTURE0);
			// Bind 2D texture to currently active texture unit (0)
			glBindTexture(GL_TEXTURE_2D, mesh->texture->textureId);
			// Tell fragment shader's "texture1" sampler to sample from texture bound to texture unit 0
			shader->setUniformInt("texture1", 0);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->IBO);
			//printGLError("glBindBuffer(ElementArray)");

			glDrawElements(GL_TRIANGLES, mesh->polygons_qty * 3, GL_UNSIGNED_INT, 0);
			printGLError("glDrawElements");
		}
	}

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}
