#pragma once
#include "Model.h"

class ResourceLoader {
public:
	char Load3DS(ModelPointer p_object, char* p_filename);
	char LoadWithAssimp(ModelPointer p_object, char* p_filename);
};