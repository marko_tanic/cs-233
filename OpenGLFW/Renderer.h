#pragma once

#include <vector>

class Camera;
class Shader;
class GameObject;

class IRenderer {
protected:
	std::vector<GameObject*> gameObjects;
	Shader* shader;
public:
	void addGameObject(GameObject* gameObject);
	void setShader(Shader* shader);
	virtual void render(Camera* camera) = 0;
};

class PhongRenderer : public IRenderer {
private:
	int pointLightOffset, spotLightOffset;
	unsigned int UBO;
	inline void setLights(glm::mat4 modelViewMatrix, glm::mat3 normalMatrix);
	inline void setMaterial(GameObject* gameObject);
public:
	virtual void render(Camera* camera) override;
};