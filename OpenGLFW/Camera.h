#pragma once
#include "glm/glm.hpp"

class GameObject;
class Shader;

class Camera {
private:
	glm::mat4 view;
	glm::mat4 projection;
	glm::vec3 position;
	glm::vec3 target;
	glm::vec3 up;
	glm::vec3 right;
	int lastMousePositionX, lastMousePositionY;
	void update();
public:
	glm::mat4 getView() { return view; }
	glm::mat4 getProjection() { return projection; }
	void lookAt(glm::vec3 position, glm::vec3 center, glm::vec3 up);
	void rotateHorizontal(float angle);
	void rotateVertical(float angle);
	void translate(glm::vec3 amount);
	void OnMouse(int x, int y);
	void OnKeyboard(char key);
	void setProjection(glm::mat4 projection);
};