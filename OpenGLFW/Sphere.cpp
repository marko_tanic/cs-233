#include "Model.h"
#include "glm/glm.hpp"
#include "glm/gtc/constants.hpp"

Model* generateSphere(int sectorCount, int stackCount) {
    float x, y, z, xy;                              // vertex position
    float nx, ny, nz, lengthInv = 1.0f;    // vertex normal
    float s, t;                                     // vertex texCoord
    float radius = 1.0f;

    float sectorStep = 2 * glm::pi<float>() / sectorCount;
    float stackStep = glm::pi<float>() / stackCount;
    float sectorAngle, stackAngle;
    Model* m = new Model();
    Mesh* modelMesh = new Mesh();
    m->meshes.push_back(modelMesh);

    for (int i = 0; i <= stackCount; ++i)
    {
        stackAngle = glm::pi<float>() / 2 - i * stackStep;        // starting from pi/2 to -pi/2
        xy = radius * cosf(stackAngle);             // r * cos(u)
        z = radius * sinf(stackAngle);              // r * sin(u)

        // add (sectorCount+1) vertices per stack
        // the first and last vertices have same position and normal, but different tex coords
        for (int j = 0; j <= sectorCount; ++j)
        {
            sectorAngle = j * sectorStep;           // starting from 0 to 2pi

            // vertex position (x, y, z)
            x = xy * cosf(sectorAngle);             // r * cos(u) * cos(v)
            y = xy * sinf(sectorAngle);             // r * cos(u) * sin(v)
            modelMesh->vertex[modelMesh->vertices_qty++] = Vertex{ x, y, z };
        }
    }

    GLuint k1, k2;
    for (int i = 0; i < stackCount; ++i)
    {
        k1 = i * (sectorCount + 1);     // beginning of current stack
        k2 = k1 + sectorCount + 1;      // beginning of next stack

        for (int j = 0; j < sectorCount; ++j, ++k1, ++k2)
        {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if (i != 0)
            {
                modelMesh->polygon[modelMesh->polygons_qty++] = IndexedPolygon{ k1, k2, k1 + 1 };
            }

            // k1+1 => k2 => k2+1
            if (i != (stackCount - 1))
            {
                modelMesh->polygon[modelMesh->polygons_qty++] = IndexedPolygon{ k1 + 1, k2, k2 + 1 };
            }
        }
    }

    return m;
}