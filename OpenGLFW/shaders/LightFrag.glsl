#version 330

struct Material {
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

// Structure that defines all needed parameters for a single directional light
struct DirectionalLight {
	vec3 direction;

	vec3 diffuse;
	vec3 specular;
};

// Structure that defines all needed parameters for a single point light
struct PointLight {
	vec3 position;

	float linearAttenuation;

	vec3 diffuse;
	vec3 specular;
};

// Define maximum number of lights per light type
#define MAX_DIRECTIONAL_LIGHTS 2
#define MAX_POINT_LIGHTS 4

// Arrays of different types of lights
uniform DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
uniform PointLight pointLights[MAX_POINT_LIGHTS];

// How many lights actually are on the scene
uniform int dirLightsCount;
uniform int pointLightsCount;

uniform Material material;

in vec3 TransformedNormal;
in vec3 MVPosition;
in vec2 TextureCoords;

// We use sampler to get color data for pixel from texture
uniform sampler2D texture1;

out vec4 FragColor; 

vec3 getPointLight(PointLight light) {
	float mDistance = length(light.position - MVPosition);
	vec3 lightVec = normalize(light.position - MVPosition);

	float attenuationFactor = 1.f / (light.linearAttenuation * mDistance);
	
	float diffuse = max(dot(lightVec, TransformedNormal), 0.0) * attenuationFactor;

	vec3 reflectVec = reflect(-lightVec, TransformedNormal);
	vec3 viewVec = normalize(-MVPosition);
	float spec = 0.0;
	if (diffuse > 0.0) {
		spec = max(dot(reflectVec, viewVec), 0.0) * attenuationFactor;
		spec = pow(spec, material.shininess);
	}

	vec3 texColor = vec3(texture2D(texture1, TextureCoords));

	return texColor * light.diffuse * material.diffuse * diffuse + texColor * light.specular * material.specular * spec;
}

vec3 getDirectionalLight(DirectionalLight light) {
	float diffuse = max(dot(light.direction, TransformedNormal), 0.0);

	vec3 reflectVec = reflect(-light.direction, TransformedNormal);
	vec3 viewVec = normalize(-MVPosition);
	float spec = 0.0;
	if (diffuse > 0.0) {
		spec = max(dot(reflectVec, viewVec), 0.0);
		spec = pow(spec, material.shininess);
	}

	vec3 texColor = vec3(texture2D(texture1, TextureCoords));

	return texColor * light.diffuse * material.diffuse * diffuse + texColor * light.specular * material.specular * spec;
}

void main() 
{
	vec3 color = vec3(0);

	for (int i = 0; i < dirLightsCount; ++i) {
		color += getDirectionalLight(directionalLights[i]);
	}

	for (int i = 0; i < pointLightsCount; ++i) {
		color += getPointLight(pointLights[i]);
	}

	FragColor = vec4(color, 1.0f);
}