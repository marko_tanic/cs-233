#version 330

uniform mat4 modelView;
uniform mat4 modelViewProjection;
uniform mat3 normalMatrix;

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 TexCoords;

out vec3 TransformedNormal;
out vec3 MVPosition;
out vec2 TextureCoords;

void main() 
{
	MVPosition = vec3(modelView * vec4(Position, 1));
	TransformedNormal = normalize(normalMatrix * Normal);
	TextureCoords = TexCoords;

	gl_Position = modelViewProjection * vec4(Position, 1.0);
}