#version 330 

uniform mat4 modelView;

layout(location = 0) in vec3 Position;
layout(location = 1) in vec4 Color;

smooth out vec4 VertexColor;

void main() 
{ 
	gl_Position = modelView * vec4(Position, 1.0);
	VertexColor = Color;
}