#version 330 

out vec4 FragColor; 

smooth in vec4 VertexColor;

void main() 
{ 
	FragColor = VertexColor; 
}