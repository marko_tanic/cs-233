#pragma once
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "glm/glm.hpp"
#include <vector>

// Max number of vertices (for each object) 
#define MAX_VERTICES 8000

// Max number of polygons (for each object) 
#define MAX_POLYGONS 8000

typedef struct {
	GLfloat x, y, z;
} Vertex;

// The polygon (triangle), 3 numbers that aim 3 vertices
typedef struct {
	GLuint a, b, c;
} IndexedPolygon;

struct Texture {
	GLuint textureId;
	GLuint width;
	GLuint height;
	void* textureData;
};

struct Mesh {
	GLuint VBO, IBO, NBO, TCBO;
	int vertices_qty = 0;
	int polygons_qty = 0;
	Vertex vertex[MAX_VERTICES];
	IndexedPolygon polygon[MAX_POLYGONS];
	glm::vec3 normals[MAX_VERTICES];
	glm::vec2 textureCoordinates[MAX_VERTICES];
	Texture* texture;
};

// The object type
struct Model {
	char name[20];
	std::vector<Mesh*> meshes;
	void Init();
	glm::vec3 getObjectCenter();
};

typedef Model * ModelPointer;
