#pragma once
#include <GL\glew.h>

class Shader {
private:
	GLuint _shaderId;
	GLuint _fragShaderId;
	GLuint _vertShaderId;
	GLchar* _fragSource = nullptr;
	GLchar* _vertSource = nullptr;
	inline GLchar* readShaderFromFile(const char* path);
public:
	void init();
	void use();
	void setUniformMat4(const char* name, float* value);
	void setUniformMat3(const char* name, float* value);
	void setUniformVec3(const char* name, float* value);
	void setUniformFloat(const char* name, float value);
	void setUniformInt(const char* name, int value);
	void readVertSource(const char* path);
	void readFragSource(const char* path);
	GLuint getShaderId() { return _shaderId; }
};