#include "GameObject.h"
#include "Renderer.h"
#include "glm/gtc/matrix_transform.hpp"

using namespace glm;

void GameObject::setPosition(vec3 newPosition) {
	position = newPosition;
}

void GameObject::setRotationLocal(float angle, glm::vec3 axis)
{
	vec3 centeredPosition = position + model->getObjectCenter();
	mat4 moveToCenterMatrix = translate(identity<mat4>(), -centeredPosition);
	mat4 rotationMatrix = rotate(identity<mat4>(), angle, axis);
	mat4 returnToPositionMatrix = translate(identity<mat4>(), centeredPosition);

	mat4 result = returnToPositionMatrix * rotationMatrix * moveToCenterMatrix;
	rotation = result;
}

void GameObject::setScaleLocal(glm::vec3 scaleLocal)
{
	vec3 centeredPosition = position + model->getObjectCenter();
	mat4 moveToCenterMatrix = translate(identity<mat4>(), -centeredPosition);
	mat4 scaleMatrix = glm::scale(identity<mat4>(), scaleLocal);
	mat4 returnToPositionMatrix = translate(identity<mat4>(), centeredPosition);

	mat4 result = returnToPositionMatrix * scaleMatrix * moveToCenterMatrix;
	scale = result;
}

void GameObject::setModel(Model* newModel) {
	model = newModel;
}

void GameObject::setRenderer(IRenderer* renderer)
{
	this->renderer = renderer;
	renderer->addGameObject(this);
}

Model* GameObject::getModel()
{
	return model;
}

mat4 GameObject::getModelMatrix() {
	mat4 localTransform = scale * rotation * translate(identity<mat4>(), position);
	if (parent != nullptr) {
		return parent->getModelMatrix() * localTransform;
	}
	else {
		return localTransform;
	}
}

void GameObject::setParent(GameObject* newParent)
{
	if (parent) {
		parent->removeChild(this);
	}
	newParent->addChild(this);
}

GameObject* GameObject::getParent()
{
	return parent;
}

void GameObject::addChild(GameObject* child)
{
	if (child->parent) {
		child->parent->removeChild(child);
	}
	child->parent = this;
	this->children.insert(child);
}

void GameObject::removeChild(GameObject* child)
{
	children.erase(child);
	child->parent = nullptr;
}

std::set<GameObject*> GameObject::getChildren()
{
	return children;
}
