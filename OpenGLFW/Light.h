#pragma once

#include "glm/glm.hpp"
#include <vector>

// Define maximum number of lights per light type
#define MAX_DIRECTIONAL_LIGHTS 2
#define MAX_POINT_LIGHTS 4
#define MAX_SPOT_LIGHTS 2

// Structure that defines all needed parameters for a single directional light
struct DirectionalLight {
	glm::vec3 direction;

	glm::vec3 diffuse;
	glm::vec3 specular;
};

// Structure that defines all needed parameters for a single point light
struct PointLight {
	glm::vec3 position;

	float linearAttenuation;

	glm::vec3 diffuse;
	glm::vec3 specular;
};

class LightManager {
private:
	// Data structures that hold lights info
	std::vector<DirectionalLight> directionalLights;
	std::vector<PointLight> pointLights;
public:
	LightManager();
	LightManager(const LightManager& other) = delete;
	LightManager(const LightManager&& other) = delete;
	void operator=(const LightManager& other) = delete;
	DirectionalLight* createDirectionalLight(glm::vec3 direction, glm::vec3 diffuse, glm::vec3 specular);
	PointLight* createPointLight(glm::vec3 position, float linearAttenuation, glm::vec3 diffuse, glm::vec3 specular);
	std::vector<DirectionalLight> getDirectionalLights() { return directionalLights; }
	std::vector<PointLight> getPointLights() { return pointLights; }
};

extern LightManager lightManager;