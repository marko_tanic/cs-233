#include "Light.h"
#include <GL/glew.h>

LightManager lightManager;

using namespace std;
using namespace glm;

DirectionalLight* LightManager::createDirectionalLight(vec3 direction, vec3 diffuse, vec3 specular)
{
	auto iterator = directionalLights.end();
	directionalLights.insert(iterator, {direction, diffuse, specular});
	return iterator._Ptr;
}

PointLight* LightManager::createPointLight(vec3 position, float linearAttenuation, vec3 diffuse, vec3 specular)
{
	auto iterator = pointLights.end();
	pointLights.insert(iterator, { position, linearAttenuation, diffuse, specular });
	return &pointLights[pointLights.size() - 1];
}

LightManager::LightManager()
{
	
}


