#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Shader.h"

using namespace glm;

namespace TypesOfBuffers {

	Shader mShader;


	GLfloat xPosition = -70.f, yPosition = -70.f;

	void printGLError(const char* marker) {
		GLenum err = glGetError();
		if (err != GL_NO_ERROR) {
			printf("%s - GL Error: %x\n", marker, err);
		}
	}

	GLuint VBO, VBOColor, IBO;
	mat4 projectionMatrix;

	void GenerateInterleavedPyramidBuffers() {
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &IBO);

		GLfloat verticesAndColors[] = { // 5 vertices of the pyramid in (x,y,z) 
		-1.0f, -1.0f, -1.0f,  // vertex 0. left-bottom-back 
		0.0f, 0.0f, 1.0f, 1.0f, // color 0. blue 
		1.0f, -1.0f, -1.0f,  // vertex 1. right-bottom-back
		0.0f, 1.0f, 0.0f, 1.0f,  // color . green 
		1.0f, -1.0f,  1.0f,  // vertex 2. right-bottom-front 
		0.0f, 0.0f, 1.0f, 1.0f,  // color 2. blue 
		-1.0f, -1.0f,  1.0f,  // vertex 3. left-bottom-front 
		0.0f, 1.0f, 0.0f, 1.0f,  // color 3. green 
		0.0f,  1.0f,  0.0f,   // vertex 4. top 
		1.0f, 0.0f, 0.0f, 1.0f   // color 4. red
		};

		GLuint indices[] = { // Vertex indices of the 4 Triangles 
		2, 4, 3,   // front face (CCW) 
		1, 4, 2,   // right face 
		0, 4, 1,   // back face 
		4, 0, 3    // left face 
		};

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 35 * sizeof(GLfloat), verticesAndColors, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 12 * sizeof(GLuint), indices, GL_STATIC_DRAW);
	}

	void renderInterleavedPyramid() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		mShader.use();

		mat4 modelView = translate(identity<mat4>(), vec3(1.5f, 0.0f, -7.0f));

		mat4 modelViewProjection = projectionMatrix * modelView;
		mShader.setUniformMat4("modelView", &modelViewProjection[0][0]);

		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), 0);

		glEnableVertexAttribArray(1);

		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

		glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		glutSwapBuffers();
	}

	void GeneratePyramidBuffers() {
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &VBOColor);
		glGenBuffers(1, &IBO);

		GLfloat vertices[] = { // 5 vertices of the pyramid in (x,y,z) 
		-1.0f, -1.0f, -1.0f,  // 0. left-bottom-back 
		1.0f, -1.0f, -1.0f,  // 1. right-bottom-back 
		1.0f, -1.0f,  1.0f,  // 2. right-bottom-front 
		-1.0f, -1.0f,  1.0f,  // 3. left-bottom-front 
		0.0f,  1.0f,  0.0f   // 4. top 
		};

		GLfloat colors[] = {  // Colors of the 5 vertices in RGBA 
			0.0f, 0.0f, 1.0f, 1.0f,  // 0. blue 
			0.0f, 1.0f, 0.0f, 1.0f,  // 1. green 
			0.0f, 0.0f, 1.0f, 1.0f,  // 2. blue 
			0.0f, 1.0f, 0.0f, 1.0f,  // 3. green 
			1.0f, 0.0f, 0.0f, 1.0f   // 4. red
		};

		GLuint indices[] = { // Vertex indices of the 4 Triangles 
		2, 4, 3,   // front face (CCW) 
		1, 4, 2,   // right face 
		0, 4, 1,   // back face 
		4, 0, 3    // left face 
		};

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 15 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, VBOColor);
		glBufferData(GL_ARRAY_BUFFER, 20 * sizeof(GLfloat), colors, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 12 * sizeof(GLuint), indices, GL_STATIC_DRAW);
	}

	void renderPyramid() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		mShader.use();

		mat4 modelView = translate(identity<mat4>(), vec3(1.5f, 0.0f, -7.0f));

		mat4 modelViewProjection = projectionMatrix * modelView;
		mShader.setUniformMat4("modelView", &modelViewProjection[0][0]);

		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, VBOColor);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

		glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		glutSwapBuffers();
	}

	void reshapeForPyramid(int x, int y) {
		glViewport(0, 0, x, y);
		projectionMatrix = perspective(45.0f, (float)x / float(y), 0.1f, 100.0f);
		glutPostRedisplay();
	}

	// Rename to 'main' to run the example
	int exampleMain(int argc, char** argv)
	{
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
		glutInitWindowSize(1024, 768);
		glutInitWindowPosition(100, 100);
		glutCreateWindow("Tutorial 04");
		glutDisplayFunc(renderInterleavedPyramid);
		glutReshapeFunc(reshapeForPyramid);
		glEnable(GL_DEPTH_TEST);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glEnable(GL_BLEND);
		glPolygonMode(GL_FRONT, GL_FILL);
		glPolygonMode(GL_BACK, GL_LINE);
		// Must be done after glut is initialized!
		GLenum res = glewInit();
		if (res != GLEW_OK) {
			fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
			return 1;
		}
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GenerateInterleavedPyramidBuffers();
		mShader.readVertSource("shaders/VertColored.glsl");
		mShader.readFragSource("shaders/FragColored.glsl");
		mShader.init();
		glutMainLoop();
		return 0;
	}
}