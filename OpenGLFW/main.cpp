#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "ResourceLoader.h"
#include "Shader.h"
#include "Model.h"
#include "Camera.h"
#include "GameObject.h"
#include "Light.h"
#include "Renderer.h"

using namespace glm;

ResourceLoader resourceLoader;
Shader mShader;
Camera camera;

GameObject chessPawn, chessPawnChild;


GLfloat xPosition = -70.f, yPosition = -70.f;

void printGLError(const char* marker) {
	GLenum err = glGetError();
	if (err != GL_NO_ERROR) {
		printf("%s - GL Error: %x\n", marker, err);
	}
}

GameObject snowBallMan;
GameObject robot;

PhongRenderer phongRenderer;
PointLight* pointLight;
DirectionalLight* dirLight;

float lightSourceY = 500.f;

static void RenderSceneCB()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	phongRenderer.render(&camera);
	glutSwapBuffers();
}

void mouse(int x, int y, int z, int a) {

}

float rotation = 0.f;
float scaleX = 1, scaleY = 1, scaleZ = 1;
float rotationChildY = 0.f;

void keyboard(unsigned char c, int x, int y) {
	camera.OnKeyboard(c);
	switch (c)
	{
	//case 'w': xPosition += 1.5f; break;
	//case 's': xPosition -= 1.5f; break;
	//case 'd': yPosition += 1.5f; break;
	//case 'a': yPosition -= 1.5f; break;
	case 'b': pointLight->position.x += 10.1f; break; 
	case 'n': pointLight->position.x -= 10.1f; break;
	case 'g': scaleY += 0.1f; break;
	case 'h': scaleY -= 0.1f; break;
	case 'l': rotationChildY += 0.1f; break;
	case 'k': rotationChildY -= 0.1f; break;
	default: return;
	}
	printf("X Position: %f, Y Position: %f\n", xPosition, yPosition);
	//chessPawn.setPosition(vec3(xPosition, yPosition, 0));
	//chessPawn.setRotationLocal(rotation, vec3(0.f, 1.f, 0.f));
	//chessPawn.setScaleLocal(vec3(scaleX, scaleY, scaleZ));
	//chessPawnChild.setPosition(vec3(105.f, 0.f, 0.f));
	//chessPawnChild.setRotationLocal(rotationChildY, vec3(0.f, 1.f, 0.f));
	//chessPawnChild.setScaleLocal(vec3(0.5f, 0.5f, 0.5f));
	//snowBallMan.setRotationLocal(rotation, vec3(0.f, 0.f, 1.f));
	glutPostRedisplay();
}

void reshape(int x, int y) {
	camera.lookAt(vec3(-300, 0, 0), vec3(0, 0, 0), vec3(0, 1, 0));
	camera.setProjection(perspective(radians(90.0f), (float)x / (float)y, 0.1f, 550.0f));
	//camera.setProjection(ortho( - x / 2.f, x / 2.f, y / 2.f, - y / 2.f, 1.f, 500.f));
}

Model model;

void passiveMouseFunc(int x, int y) {
	camera.OnMouse(x, y);
	glutPostRedisplay();
}

extern Model* generateSphere(int sectorCount, int stackCount);
Model* sphere;

void initRobot() {
	Model* robotModel = new Model();
	resourceLoader.LoadWithAssimp(robotModel, "typhoon1.fbx");
	robotModel->Init();
	robot.setModel(robotModel);
	robot.setScaleLocal(vec3(30.f, 30.f, 30.f));
	robot.setRotationLocal(1.5f, vec3(0.f, 0.f, 1.f));
	robot.setRenderer(&phongRenderer);
	robot.getMaterial()->diffuse = vec3(0.5f, 0.5f, 0.1f);
	robot.getMaterial()->shininess = 1.f;
}

void initLights() {
	pointLight = lightManager.createPointLight(vec3(400.f, 400.f, 400.f), 0.0001f, vec3(0.7f, 0.3f, 0.7f), vec3(0.7f, 0.3f, 0.7f));
	dirLight = lightManager.createDirectionalLight(vec3(0.6f, 0.6f, 0.f), vec3(0.3f, 0.9f, 0.9f), vec3(0.1f, 0.9f, 0.6f));
}

void initSnowBallMan() {
	sphere = generateSphere(30, 30);
	sphere->Init();
	snowBallMan.setModel(sphere);
	snowBallMan.setScaleLocal(vec3(50.f, 50.f, 50.f));
	GameObject* middle = new GameObject();
	middle->setModel(sphere);
	middle->setParent(&snowBallMan);
	middle->setScaleLocal(vec3(0.7f, 0.7f, 0.7f));
	middle->setPosition(vec3(0.f, 1.7f, 0.f));
	GameObject* head = new GameObject();
	head->setModel(sphere);
	head->setParent(middle);
	head->setScaleLocal(vec3(0.7f, 0.7f, 0.7f));
	head->setPosition(vec3(0.f, 1.7f, 0.f));
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(1024, 768);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Tutorial 04");
	glutPassiveMotionFunc(passiveMouseFunc);
	glutWarpPointer(512, 384);
	glutDisplayFunc(RenderSceneCB);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glEnable(GL_DEPTH_TEST);
	// Enable 2D textures
	glEnable(GL_TEXTURE_2D);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glEnable(GL_BLEND);
	glCullFace(GL_BACK);
	glPolygonMode(GL_FRONT, GL_FILL);
	//glPolygonMode(GL_BACK, GL_LINE);
	// Must be done after glut is initialized!
	GLenum res = glewInit();
	if (res != GLEW_OK) {
		fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
		return 1;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	
	mShader.readVertSource("shaders/LightVert.glsl");
	mShader.readFragSource("shaders/LightFrag.glsl");
	mShader.init();
	phongRenderer.setShader(&mShader);

	initRobot();
	initLights();

	glutMainLoop();
	return 0;
}
