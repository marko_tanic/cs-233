#pragma once
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "Model.h"
#include <set>

class IRenderer;

struct Material {
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;
};

class GameObject {
private:
	GameObject* parent = nullptr;
	std::set<GameObject*> children;
	glm::vec3 position = glm::vec3(0.f, 0.f, 0.f);
	glm::mat4 rotation = glm::identity<glm::mat4>();
	glm::mat4 scale = glm::identity<glm::mat4>();
	Model* model;
	IRenderer* renderer;
	Material material = {glm::vec3(1.f, 1.f, 1.f), glm::vec3(1.f, 1.f, 1.f), 16};
public:
	void setPosition(glm::vec3);
	void setRotationLocal(float angle, glm::vec3 axis);
	void setScaleLocal(glm::vec3 scale);
	void setModel(Model*);
	void setRenderer(IRenderer* renderer);
	Model* getModel();
	glm::mat4 getModelMatrix();
	void setParent(GameObject* newParent);
	inline GameObject* getParent();
	void addChild(GameObject* child);
	void removeChild(GameObject* child);
	std::set<GameObject*> getChildren();
	Material* getMaterial() { return &material; };
};