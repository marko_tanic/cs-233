#include "Model.h"
#include "glm/glm.hpp"

using namespace glm;

void Model::Init() {
	for (auto mesh : meshes) {
		glGenBuffers(1, &mesh->VBO);
		glBindBuffer(GL_ARRAY_BUFFER, mesh->VBO);
		glBufferData(GL_ARRAY_BUFFER, mesh->vertices_qty * 3 * sizeof(GLfloat), mesh->vertex, GL_STATIC_DRAW);

		glGenBuffers(1, &mesh->NBO);
		glBindBuffer(GL_ARRAY_BUFFER, mesh->NBO);
		glBufferData(GL_ARRAY_BUFFER, mesh->vertices_qty * 3 * sizeof(GLfloat), mesh->normals, GL_STATIC_DRAW);

		glGenBuffers(1, &mesh->IBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->polygons_qty * 3 * sizeof(GLuint), mesh->polygon, GL_STATIC_DRAW);
		
		// Texture Coordinates buffer object
		glGenBuffers(1, &mesh->TCBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->TCBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->vertices_qty * 2 * sizeof(GLfloat), mesh->textureCoordinates, GL_STATIC_DRAW);

		// Initialize texture
		glGenTextures(1, &mesh->texture->textureId);
		glBindTexture(GL_TEXTURE_2D, mesh->texture->textureId);

		// Set texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		// Push texture to GPU
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, mesh->texture->width, mesh->texture->height, 0, GL_BGRA, GL_UNSIGNED_BYTE, mesh->texture->textureData);
	}
}

vec3 Model::getObjectCenter()
{
	vec3 result(0.f, 0.f, 0.f);
	int totalVertices = 0.f;
	for (auto mesh : meshes) {
		for (int i = 0; i < mesh->vertices_qty; ++i) {
			result += vec3(mesh->vertex[i].x, mesh->vertex[i].y, mesh->vertex[i].z);
		}
		totalVertices += mesh->vertices_qty;
	}
	return result / (float)totalVertices;
}
