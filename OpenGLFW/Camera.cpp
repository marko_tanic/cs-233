#include "Camera.h"
#include "Shader.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "GameObject.h"

using namespace glm;

extern void printGLError(const char* marker);

void Camera::update()
{
	view = glm::lookAt(position, position - target, up);
}

void Camera::lookAt(vec3 position, vec3 center, vec3 up) {
	this->position = position;
	this->target = normalize(-center + position);
	this->up = up;
	right = normalize(cross(up, target));
	update();
}

void Camera::rotateHorizontal(float angle)
{
	target = rotate(target, angle, up);
	//up = rotate(up, angle, vec3(0.f, 1.f, 0.f));
	right = normalize(cross(up, target));
	update();
}

void Camera::rotateVertical(float angle)
{
	target = rotate(target, angle, right);
	up = rotate(up, angle, right);
	//right = normalize(cross(up, target));
	update();
}

void Camera::translate(glm::vec3 amount)
{
	vec3 forwardMovement = amount.z * target;
	vec3 rightMovement = amount.x * right;
	vec3 totalMovement = forwardMovement + rightMovement;
	position += totalMovement;
	update();
}

void Camera::OnMouse(int x, int y)
{
	int deltaX = x - 512;
	float angleX =  -radians((float)deltaX) / 100.f;
	rotateHorizontal(angleX);
	int deltaY = y - 384;
	float angleY = -radians((float)deltaY) / 100.f;
	rotateVertical(angleY);
	//lastMousePositionX = x;
	glutWarpPointer(512, 384);
}

void Camera::OnKeyboard(char key)
{
	float leftRight = 0.f, forwardBackward = 0.f;
	switch (key) {
	case 'd': leftRight += 5.f; break;
	case 'a': leftRight -= 5.f; break;
	case 'w': forwardBackward -= 5.f; break;
	case 's': forwardBackward += 5.f; break;
	}
	vec3 moveAmount(leftRight, 0.f, forwardBackward);
	translate(vec3(leftRight, 0.f, forwardBackward));
}

void Camera::setProjection(mat4 projection) {
	this->projection = projection;
}