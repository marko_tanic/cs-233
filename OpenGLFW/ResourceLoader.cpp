#include "ResourceLoader.h"
#include "Model.h"
#include <stdio.h>
#include <io.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <queue>
#include "stb_image.h"

using namespace Assimp;

char ResourceLoader::Load3DS(ModelPointer p_object, char* p_filename)
{
	int i; //Index variable

	FILE* l_file; //File pointer

	unsigned short l_chunk_id; //Chunk identifier
	unsigned int l_chunk_lenght; //Chunk lenght

	unsigned char l_char; //Char variable
	unsigned short l_qty; //Number of elements in each chunk

	unsigned short l_face_flags; //Flag that stores some face information

	if ((l_file = fopen(p_filename, "rb")) == NULL) return 0; //Open the file

	Mesh* modelMesh = new Mesh();

	while (ftell(l_file) < filelength(fileno(l_file))) //Loop to scan the whole file 
	{
		//getche(); //Insert this command for debug (to wait for keypress for each chuck reading)

		fread(&l_chunk_id, 2, 1, l_file); //Read the chunk header
		//printf("ChunkID: %x\n",l_chunk_id); 
		fread(&l_chunk_lenght, 4, 1, l_file); //Read the lenght of the chunk
		//printf("ChunkLenght: %x\n",l_chunk_lenght);

		switch (l_chunk_id)
		{
			//----------------- MAIN3DS -----------------
			// Description: Main chunk, contains all the other chunks
			// Chunk ID: 4d4d 
			// Chunk Lenght: 0 + sub chunks
			//-------------------------------------------
		case 0x4d4d:
			break;

			//----------------- EDIT3DS -----------------
			// Description: 3D Editor chunk, objects layout info 
			// Chunk ID: 3d3d (hex)
			// Chunk Lenght: 0 + sub chunks
			//-------------------------------------------
		case 0x3d3d:
			break;

			//--------------- EDIT_OBJECT ---------------
			// Description: Object block, info for each object
			// Chunk ID: 4000 (hex)
			// Chunk Lenght: len(object name) + sub chunks
			//-------------------------------------------
		case 0x4000:
			i = 0;
			do
			{
				fread(&l_char, 1, 1, l_file);
				p_object->name[i] = l_char;
				i++;
			} while (l_char != '\0' && i < 20);
			break;

			//--------------- OBJ_TRIMESH ---------------
			// Description: Triangular mesh, contains chunks for 3d mesh info
			// Chunk ID: 4100 (hex)
			// Chunk Lenght: 0 + sub chunks
			//-------------------------------------------
		case 0x4100:
			break;

			//--------------- TRI_VERTEXL ---------------
			// Description: Vertices list
			// Chunk ID: 4110 (hex)
			// Chunk Lenght: 1 x unsigned short (number of vertices) 
			//             + 3 x float (assimpVertex coordinates) x (number of vertices)
			//             + sub chunks
			//-------------------------------------------
		case 0x4110:
			fread(&l_qty, sizeof(unsigned short), 1, l_file);
			modelMesh->vertices_qty = l_qty;
			//printf("Number of vertices: %d\n",l_qty);
			for (i = 0; i < l_qty; i++)
			{

				fread(&modelMesh->vertex[i].x, sizeof(float), 1, l_file);
				//printf("Vertices list x: %f\n",p_object->assimpVertex[i].x);

				fread(&modelMesh->vertex[i].y, sizeof(float), 1, l_file);
				//printf("Vertices list y: %f\n",p_object->assimpVertex[i].y);

				fread(&modelMesh->vertex[i].z, sizeof(float), 1, l_file);
				//printf("Vertices list z: %f\n",p_object->assimpVertex[i].z);

				//Insert into the database

			}
			break;

			//--------------- TRI_FACEL1 ----------------
			// Description: Polygons (faces) list
			// Chunk ID: 4120 (hex)
			// Chunk Lenght: 1 x unsigned short (number of polygons) 
			//             + 3 x unsigned short (polygon points) x (number of polygons)
			//             + sub chunks
			//-------------------------------------------
		case 0x4120:
			fread(&l_qty, sizeof(unsigned short), 1, l_file);
			modelMesh->polygons_qty = l_qty;
			//printf("Number of polygons: %d\n",l_qty); 
			for (i = 0; i < l_qty; i++)
			{
				fread(&modelMesh->polygon[i].a, sizeof(unsigned short), 1, l_file);
				//printf("Polygon point a: %d\n",p_object->polygon[i].a);
				fread(&modelMesh->polygon[i].b, sizeof(unsigned short), 1, l_file);
				//printf("Polygon point b: %d\n",p_object->polygon[i].b);
				fread(&modelMesh->polygon[i].c, sizeof(unsigned short), 1, l_file);
				//printf("Polygon point c: %d\n",p_object->polygon[i].c);
				fread(&l_face_flags, sizeof(unsigned short), 1, l_file);
				//printf("Face flags: %x\n",l_face_flags);
			}
			break;

			//------------- TRI_MAPPINGCOORS ------------
			// Description: Vertices list
			// Chunk ID: 4140 (hex)
			// Chunk Lenght: 1 x unsigned short (number of mapping points) 
			//             + 2 x float (mapping coordinates) x (number of mapping points)
			//             + sub chunks
			//-------------------------------------------
			//----------- Skip unknow chunks ------------
			//We need to skip all the chunks that currently we don't use
			//We use the chunk lenght information to set the file pointer
			//to the same level next chunk
			//-------------------------------------------
		default:
			fseek(l_file, l_chunk_lenght - 6, SEEK_CUR);
		}
	}
	p_object->meshes.push_back(modelMesh);

	fclose(l_file); // Closes the file stream

	return (1); // Returns ok

}

inline aiMatrix4x4 getGlobalTransform(aiNode* node) {
	if (node->mParent) {
		return getGlobalTransform(node->mParent) * node->mTransformation;
	} 
	else {
		return node->mTransformation;
	}
}

char ResourceLoader::LoadWithAssimp(ModelPointer p_object, char* p_filename)
{
	Importer importer;

	const aiScene* scene = importer.ReadFile(p_filename, aiProcess_Triangulate | aiProcess_GenSmoothNormals |
		aiProcess_FlipUVs | aiProcess_JoinIdenticalVertices | aiProcess_PreTransformVertices);
	
	std::queue<aiNode*> nodeQueue;
	nodeQueue.push(scene->mRootNode);
	while (!nodeQueue.empty()) {
		aiNode* currentNode = nodeQueue.front();
		nodeQueue.pop();
		aiMatrix4x4 transform = getGlobalTransform(currentNode);
		for (int i = 0; i < currentNode->mNumMeshes; ++i) {
			aiMesh* currentMesh = scene->mMeshes[currentNode->mMeshes[i]];
			bool hasTC = currentMesh->HasTextureCoords(0);
			Mesh* modelMesh = new Mesh();
			for (int v = 0; v < currentMesh->mNumVertices; ++v) {
				aiVector3D currentVertex = transform * currentMesh->mVertices[v];
				modelMesh->vertex[modelMesh->vertices_qty++] = Vertex{ currentVertex.x, currentVertex.y, currentVertex.z };
			}
			for (int p = 0; p < currentMesh->mNumFaces; ++p) {
				auto currentFace = currentMesh->mFaces[p];
				modelMesh->polygon[modelMesh->polygons_qty++] = IndexedPolygon{
					currentFace.mIndices[0]
					, currentFace.mIndices[1]
					, currentFace.mIndices[2] };
			}
			for (int n = 0; n < currentMesh->mNumVertices; ++n) {
				aiVector3D currentNormal = currentMesh->mNormals[n];
				modelMesh->normals[n] = glm::vec3{currentNormal.x, currentNormal.y, currentNormal.z};
			}
			// Load texture coordinates
			for (int tc = 0; tc < currentMesh->mNumVertices; ++tc) {
				aiVector3D currentTexCoord = currentMesh->mTextureCoords[0][tc];
				modelMesh->textureCoordinates[tc] = glm::vec2{ currentTexCoord.x, currentTexCoord.y };
			}
			// Load texture. Note: this code assumes that textures are embedded
			aiMaterial* material = scene->mMaterials[currentMesh->mMaterialIndex];
			aiString textureFile;
			material->Get(AI_MATKEY_TEXTURE(aiTextureType_DIFFUSE, 0), textureFile);
			const aiTexture* texture = scene->GetEmbeddedTexture(textureFile.C_Str());
			Texture* modelTexture = new Texture();
			modelMesh->texture = modelTexture;
			int nrChanels;
			// Convert from tga to RGBA format using STBI library
			modelTexture->textureData = stbi_load_from_memory(reinterpret_cast<stbi_uc*>(texture->pcData), texture->mWidth, (int*)&modelTexture->width, (int*)&modelTexture->height, &nrChanels, 4);
			p_object->meshes.push_back(modelMesh);
		}
		for (int c = 0; c < currentNode->mNumChildren; ++c) {
			nodeQueue.push(currentNode->mChildren[c]);
		}

	}
	return 0;
}
