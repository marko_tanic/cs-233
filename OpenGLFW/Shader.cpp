#include "Shader.h"
#include <GL/freeglut.h>
#include <exception>
#include <string.h>
#include <fstream>
#include <iostream>

using namespace std;

inline GLchar* Shader::readShaderFromFile(const char* path)
{
	ifstream file;
	int length;
	file.open(path, ios::in);      // open input file
	file.seekg(0, std::ios::end);    // go to the end
	length = file.tellg();           // report location (this is the length)
	file.seekg(0, std::ios::beg);    // go back to the beginning
	char* buffer = new char[length+1];    // allocate memory for a buffer of appropriate dimension
	file.read(buffer, length);       // read the whole file into the buffer
	file.close();
	buffer[length] = '\0';
	return buffer;
}

void Shader::init() {
	if (!_vertSource || !_fragSource) {
		throw exception("Vertex or fragment source code not set");
	}
	_shaderId = glCreateProgram();

	{
		_vertShaderId = glCreateShader(GL_VERTEX_SHADER);
		if (_vertShaderId == 0) {
			fprintf(stderr, "Error creating vertex shader\n");
			exit(0);
		}
		const GLchar* p[1];
		p[0] = _vertSource;
		GLint length[1];
		length[0] = strlen(_vertSource);
		glShaderSource(_vertShaderId, 1, p, length);
		GLint success;
		glCompileShader(_vertShaderId);
		glGetShaderiv(_vertShaderId, GL_COMPILE_STATUS, &success);
		if (!success) {
			GLchar InfoLog[1024];
			glGetShaderInfoLog(_vertShaderId, 1024, NULL, InfoLog);
			fprintf(stderr, "Error compiling vertex shader: '%s'\n", InfoLog);
			exit(1);
		}
		glAttachShader(_shaderId, _vertShaderId);
	}
	{
		_fragShaderId = glCreateShader(GL_FRAGMENT_SHADER);
		if (_fragShaderId == 0) {
			fprintf(stderr, "Error creating fragment shader\n");
			exit(0);
		}
		const GLchar* p[1];
		p[0] = _fragSource;
		GLint length[1];
		length[0] = strlen(_fragSource);
		glShaderSource(_fragShaderId, 1, p, length);
		GLint success;
		glCompileShader(_fragShaderId);
		glGetShaderiv(_fragShaderId, GL_COMPILE_STATUS, &success);
		if (!success) {
			GLchar InfoLog[1024];
			glGetShaderInfoLog(_fragShaderId, 1024, NULL, InfoLog);
			fprintf(stderr, "Error compiling fragment shader: '%s'\n", InfoLog);
			exit(1);
		}
		glAttachShader(_shaderId, _fragShaderId);
	}

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };
	glLinkProgram(_shaderId);
	glGetProgramiv(_shaderId, GL_LINK_STATUS, &Success);
	if (Success == 0) {
		glGetProgramInfoLog(_shaderId, sizeof(ErrorLog), NULL, ErrorLog);
		fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
		exit(1);
	}
	glValidateProgram(_shaderId);
	glGetProgramiv(_shaderId, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(_shaderId, sizeof(ErrorLog), NULL, ErrorLog);
		fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
		exit(1);
	}
}

void Shader::use() {
	glUseProgram(_shaderId);
}

void Shader::setUniformMat4(const char* name, float* value)
{
	GLint loc = glGetUniformLocation(_shaderId, name);
	if (loc != -1)
	{
		glUniformMatrix4fv(loc, 1, false, value);
	}
}

void Shader::setUniformMat3(const char* name, float* value)
{
	GLint loc = glGetUniformLocation(_shaderId, name);
	if (loc != -1)
	{
		glUniformMatrix3fv(loc, 1, false, value);
	}
}

void Shader::setUniformVec3(const char* name, float* value)
{
	GLint loc = glGetUniformLocation(_shaderId, name);
	if (loc != -1)
	{
		glUniform3fv(loc, 1, value);
	}
}

void Shader::setUniformFloat(const char* name, float value)
{
	GLint loc = glGetUniformLocation(_shaderId, name);
	if (loc != -1)
	{
		glUniform1f(loc, value);
	}
}

void Shader::setUniformInt(const char* name, int value)
{
	GLint loc = glGetUniformLocation(_shaderId, name);
	if (loc != -1)
	{
		glUniform1i(loc, value);
	}
}

void Shader::readVertSource(const char* path)
{
	_vertSource = readShaderFromFile(path);
}

void Shader::readFragSource(const char* path)
{
	_fragSource = readShaderFromFile(path);
}
